import posix
import sys
import string

def curl_tg(opts):
	posix.system('curl -vv ' + opts)

def send_message(chat_id, text_id):
	method_url = '/sendMessage'
	chat_id_fs = ' --form-string "chat_id=' + chat_id + '"'
	user_text_fs = ' --form-string "text=' + text_id + '"'
	curl_opts = api + method_url + chat_id_fs + user_text_fs
	curl_tg(curl_opts)

def mkarg(x):
	if '\'' not in x:
		return ' \'' + x + '\''
	s = ' "'
	for c in x:
		if c in '\\$"':
			s = s + '\\'
		s = s + c
	s = s + '"'
	return s

def readfile(fn):
	size = posix.stat(fn)[6]
	fp = open(fn, 'r')
	return fp.read(size)

def update_id():
	out_file = 'up_id'
	posix.system('echo $(($RANDOM % 1000)) > ' + out_file)
	data = readfile(out_file)[:-1]
	posix.system('rm -f ' + out_file)
	return data

def readsys(cmd):
	out_file = update_id()
	posix.system(cmd + ' > ' + out_file)
	data = readfile(out_file)[:-1]
	posix.system('rm -f ' + out_file)
	return data

def get_json(field):
	out_file = update_id()
	cmd = 'jshon -Q ' + field + ' <<< ' + mkarg(json) + ' > ' + out_file + ' 2>&1'
	cmd = posix.system(cmd)
	data = readfile(out_file)[:-1]
	posix.system('rm -f ' + out_file)
	return data

def main():
	chat_id = get_json('-e message -e chat -e id -u')
	user_text = get_json('-e message -e text -u')
	if user_text = 'a':
		text_id = user_text
		send_message(chat_id, text_id)
	elif user_text = 'A':
		text_id = user_text
		send_message(chat_id, text_id)
	elif user_text = '!ping':
		ping = readsys('ping -c 1 api.telegram.org | sed -En "s/.*time=(.*)/\\1/p"')
		text_id = 'pong\napi: ' + ping + '\n'
		response_time = readsys('bc <<< "' + '$(date +%s)' + ' - ' + get_json('-e message -e date -u') + '"')
		text_id = text_id + 'response time: ' + response_time + ' s\n'
		script_time = readsys('bc <<< "($(date +%s%N) / 1000000) - ' + start_time + '"')
		text_id = text_id + 'script time: ' + script_time + ' ms'
		send_message(chat_id, text_id)

json = sys.argv[1]
start_time = readsys('bc <<< "$(date +%s%N) / 1000000"')
token = readfile('token')[:-1]
api = 'http://192.168.1.15:8081/bot' + token

main()
